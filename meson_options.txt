option('libvlc', type: 'feature', value: 'auto')
option('libjpeg_prefix', type: 'string')
option('tests', type: 'feature', value: 'auto')
option('force_attachment_api', type: 'boolean', value: false)
option('fuzz', type: 'boolean', value: false)
option('libtool_workaround', type: 'boolean', value: false,
  description: 'Force explicit mention of some libraries in the .pc ' +
  'file that would be overwritten by libtool\'s use of -nostdlib')
